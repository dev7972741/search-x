<div align="center">

  <h3 align="center">Search X</h3>

  <p align="center">
    Search bar component built without the usage of any external libraries
    <br />
  </p>

  <img src="docs/images/demo.gif" alt="Demo of the search">

</div>


## About The Project

Search bar that was built under 10 hours using only React and built-in hooks.

Due to the set time constraints, some things are just done to a working prototype stage.

Please note that there may be some bugs.

### Built With

This project is built entirely with React (with the usage of Create React App with Typescript template). Not a single external 3rd party library was used.

[![React][React.js]][React-url]

<!-- GETTING STARTED -->
## Getting Started

* run `yarn` in the root folder of the project
* run `yarn start` to start a `dev` server


<!-- USAGE EXAMPLES -->
## Technical Decisions

### Data

It was decided to proceed storing data in `localStorage`. The access to API was out of scope due to allocated time and all the manipulations with data are done on FE

The data itself is represented by an array of 3 search terms (React, HTML and Javascript) and every array contains 10 sample records (e.g. `react native`, `react redux`, etc.).

Every search term contains the same link for every element of the array and description for search result is generated using random substring from "Lorem Ipsum" text (so that it's dynamic).

### State management

React Context is used to manage state. It's built-in (so no external dependencies) and has a dedicated hooks for interactions with it.

### Separation of functionality into domains

For that purpose, functionality is hidden in a separate hooks (e.g. `useSearchSuggestions`). The separation most probably is not optimal, but it's good that it's there from the start so functionality can be refactored later.

### Components

UI was splitted into a separate components so that UI logic is encapsulated. The styling was done using regular `.css` files with defined class selectors. Shortcoming of this approach is that the styles are leaking to global domain, but the good thing is that zero setup was required for it.

### Project setup

Though a proper ESLint setup would greatly simplify the code redability and formatting, it wasn't setup due to the time constraints.

### Type safety

Not applied to all the project, but the available queries are typed so that there autocomplete for code and type checking for it.

## License

Distributed under the MIT License.


[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/