import { LOREM_IPSUM_TEXT } from '../data';

export const generateRandomText = (initialPosition?: number, lastPosition?: number) => {
  const RANDOM_TEXT_LENGTH = 50;
  const firstPosition =
    initialPosition ??
    Math.floor(Math.random() * (LOREM_IPSUM_TEXT.length - RANDOM_TEXT_LENGTH)) + 1;
  const secondPosition = lastPosition ?? RANDOM_TEXT_LENGTH;
  return LOREM_IPSUM_TEXT.slice(firstPosition, firstPosition + secondPosition);
};
