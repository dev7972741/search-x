import './App.css';
import { QueryProvider } from './contexts';
import { SearchContainer } from './layouts/search-container';

function App() {
  return (
    <div className="App">
      <QueryProvider>
        <main className='mainContainer'>
          <SearchContainer/>
        </main>
      </QueryProvider>
    </div>
  );
}

export default App;
