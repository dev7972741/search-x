import React, { createContext, useState } from 'react';

import type { AvailableQuery, QueryContextType } from '../types';

export const QueryContext = createContext<QueryContextType>({
  availableQueries: [],
  selectedQuery: undefined,
  suggestionsList: [],
  setAvailableQueries: () => {},
  setSuggestionsList: () => {},
  setSelectedQuery: () => {},
});

export const QueryProvider = ({ children }: { children: any }) => {
  const [availableQueries, setAvailableQueries] = useState<AvailableQuery[]>([]);
  const [suggestionsList, setSuggestionsList] = useState<AvailableQuery[]>([]);
  const [selectedQuery, setSelectedQuery] = useState<AvailableQuery>();

  return (
    <QueryContext.Provider
      value={{
        availableQueries,
        setAvailableQueries,
        selectedQuery,
        setSuggestionsList,
        suggestionsList,
        setSelectedQuery,
      }}
    >
      {children}
    </QueryContext.Provider>
  );
};
