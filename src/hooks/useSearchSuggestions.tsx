import { useContext } from 'react';

import { QueryContext } from '../contexts';
import type { AvailableQuery } from '../types';

export const useSearchSuggestions = () => {
  const queryProviderContext = useContext(QueryContext);

  const fetchSearchSuggestions = (value: string) => {
    const matchingQueries = queryProviderContext.availableQueries?.filter((item: AvailableQuery) =>
      item?.title.includes(value.toLowerCase()),
    );
    return [
      ...matchingQueries.filter((x) => x?.viewed),
      ...matchingQueries.filter((x) => !x?.viewed),
    ].slice(0, 10);
  };

  return {
    fetchSearchSuggestions,
  };
};
