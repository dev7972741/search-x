import { useContext, useEffect } from 'react';

import { QueryContext } from '../contexts';
import {
  AVAILABLE_HTML_QUERIES,
  AVAILABLE_JAVASCRIPT_QUERIES,
  AVAILABLE_REACT_QUERIES,
} from '../data';

export const useLocalStorage = () => {
  const queryProviderContext = useContext(QueryContext);

  useEffect(() => {
    localStorage.setItem(
      'availableQueries',
      JSON.stringify([
        ...AVAILABLE_REACT_QUERIES,
        ...AVAILABLE_JAVASCRIPT_QUERIES,
        ...AVAILABLE_HTML_QUERIES,
      ]),
    );
    queryProviderContext.setAvailableQueries([
      ...AVAILABLE_REACT_QUERIES,
      ...AVAILABLE_JAVASCRIPT_QUERIES,
      ...AVAILABLE_HTML_QUERIES,
    ]);
  }, []);

  const updateRecord = (title: string, viewed: boolean) => {
    const updatedAvailableQueries = queryProviderContext.availableQueries.map((item: any) =>
      item.title === title ? { ...item, viewed } : item,
    );
    localStorage.setItem('availableQueries', JSON.stringify(updatedAvailableQueries));
    queryProviderContext.setAvailableQueries(updatedAvailableQueries);
  };

  return {
    updateRecord,
  };
};
