export type AvailableQuery = {title: string, viewed?: boolean, link?: string} | undefined

export type QueryContextType = {
    availableQueries: AvailableQuery[],
    selectedQuery: AvailableQuery,
    suggestionsList: AvailableQuery[],
    setAvailableQueries: (queries: AvailableQuery[]) => void,
    setSuggestionsList: (list: AvailableQuery[]) => void,
    setSelectedQuery: (item: AvailableQuery) => void,
}