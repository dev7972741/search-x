import './styles.css';

import { useContext } from 'react';

import { InputWithAutocomplete } from '../../components/input-with-autocomplete';
import { ResultsItem } from '../../components/results-item';
import { ResultsList } from '../../components/results-list';
import { QueryContext } from '../../contexts';

export const SearchContainer = () => {
  const queryProviderContext = useContext(QueryContext);

  return (
    <div className="searchContainer">
      <InputWithAutocomplete autoFocus={true} />
      {queryProviderContext.selectedQuery && <p className="numberOfResults">Results: around 5</p>}
      <ResultsList>
        {queryProviderContext.selectedQuery &&
          new Array(5)
            .fill(undefined)
            .map((x: any, index: number) => (
              <ResultsItem
                link={queryProviderContext.selectedQuery?.link!}
                title={queryProviderContext.selectedQuery?.title!}
              />
            ))}
      </ResultsList>
    </div>
  );
};
