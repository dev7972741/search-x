export const AVAILABLE_REACT_QUERIES = [
    {
        title: 'react native',
        link: 'https://reactnative.dev/'
    },
    {
        title: 'react js',
        link: 'https://reactnative.dev/'
    },
    {
        title: 'react router',
        link: 'https://reactnative.dev/'
    },
    {
        title: 'react redux',
        link: 'https://reactnative.dev/'
    },
    {
        title: 'react query',
        link: 'https://reactnative.dev/'
    },
    {
        title: 'react vs react native',
        link: 'https://reactnative.dev/'
    },
    {
        title: 'react bootstrap',
        link: 'https://reactnative.dev/'
    },
    {
        title: 'react hooks',
        link: 'https://reactnative.dev/'
    },
    {
        title: 'react devtools',
        link: 'https://reactnative.dev/'
    },
    {
        title: 'react github',
        link: 'https://reactnative.dev/'
    },
]

export const AVAILABLE_JAVASCRIPT_QUERIES = [
    {
        title: 'javascript',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
    {
        title: 'javascript online',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
    {
        title: 'javascript if',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
    {
        title: 'javascript download',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
    {
        title: 'javascript void 0 )',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
    {
        title: 'javascript map',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
    {
        title: 'javascript foreach',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
    {
        title: 'javascript regex',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
    {
        title: 'javascript api',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
    {
        title: 'javascript language',
        link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript?retiredLocale=pl'
    },
]



export const AVAILABLE_HTML_QUERIES = [
    {
        title: 'html',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
    {
        title: 'html checkbox',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
    {
        title: 'html compiler',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
    {
        title: 'html to pdf',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
    {
        title: 'html 5',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
    {
        title: 'html syntax',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
    {
        title: 'html to react',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
    {
        title: 'html pdf',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
    {
        title: 'html table',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
    {
        title: 'html tag',
        link: 'https://developer.mozilla.org/en-US/docs/Web/HTML'
    },
]

export const LOREM_IPSUM_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."