import './styles.css';

import { generateRandomText } from '../../utils';

export const ResultsItem = ({ link, title }: { link: string; title: string }) => {
  return (
    <li className="resultsItem">
      <a className="resultsItemLinkContainer" href={link} target="_blank" rel="noopener noreferrer">
        <p className="resultsItemLink">{link}</p>
        <h3 className="resultsItemTitle">{`${title} - ${generateRandomText(undefined, 5)}`}</h3>
        <p className="resultsItemDescription">{generateRandomText()}</p>
      </a>
    </li>
  );
};
