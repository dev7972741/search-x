import './styles.css';

export const ResultsList = ({ children, ...rest }: { children: any }) => {
  return (
    <ul className="resultsList" {...rest}>
      {children}
    </ul>
  );
};
