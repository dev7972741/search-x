import './styles.css';

export const AutocompleteItem = ({
  children,
  isAlreadyViewed,
  onClick,
  onClear,
  ...rest
}: {
  children: any;
  isAlreadyViewed?: boolean;
  onClick: () => void;
  onClear: () => void;
}) => {
  return (
    <li
      className={`${isAlreadyViewed ? 'viewedAutocompleteItem' : ''} autocompleteItem`}
      onClick={onClick}
      {...rest}
    >
      <div className="autocompleteItemIcon" />
      {children}
      {isAlreadyViewed && (
        <button className="autocompleteClearButton" onClick={onClear}>
          Clear
        </button>
      )}
    </li>
  );
};
