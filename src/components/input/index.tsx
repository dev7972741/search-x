import './styles.css';

export const Input = ({ ...rest }) => <input className="input" {...rest}></input>;
