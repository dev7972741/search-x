import './styles.css';

import { useContext, useState } from 'react';

import { QueryContext } from '../../contexts';
import { useLocalStorage } from '../../hooks/useLocalStorage';
import { useSearchSuggestions } from '../../hooks/useSearchSuggestions';
import type { AvailableQuery } from '../../types';
import { AutocompleteItem } from '../autocomplete-item';
import { AutocompleteList } from '../autocomplete-list';
import { Input } from '../input';

export const InputWithAutocomplete = ({ ...rest }) => {
  const [shouldShowAutocomplete, setShouldShowAutocomplete] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState('');

  const { fetchSearchSuggestions } = useSearchSuggestions();

  const { updateRecord } = useLocalStorage();

  const queryProviderContext = useContext(QueryContext);

  const getNewSearchSuggestions = (query: string) => {
    const newSearchSuggestions = fetchSearchSuggestions(query) ?? [];
    queryProviderContext.setSuggestionsList(newSearchSuggestions);
  };

  const onChangeHandler = (e: any) => {
    setInputValue(e.target.value);
    if (e.target.value === '') {
      queryProviderContext.setSuggestionsList([]);
      queryProviderContext.setSelectedQuery(undefined);
    } else {
      getNewSearchSuggestions(e.target.value);
    }
  };

  const onAutocompleteItemClick = (item: AvailableQuery) => {
    markItemAsViewed(item?.title ?? '');
    queryProviderContext.setSelectedQuery(item);
    setInputValue(item?.title!);
  };

  const markItemAsViewed = (query: string) => {
    updateRecord(query, true);
    setShouldShowAutocomplete(false);
    getNewSearchSuggestions(query);
  };

  const clearItemSelection = (query: string) => {
    updateRecord(query, false);
    setShouldShowAutocomplete(false);
    getNewSearchSuggestions(query);
    setInputValue('');
  };

  return (
    <div className="inputWithAutocomplete">
      <Input
        value={inputValue}
        onChange={onChangeHandler}
        onFocus={() => {
          setShouldShowAutocomplete(true);
        }}
        {...rest}
      />
      <AutocompleteList shouldHideAutocompleteList={!shouldShowAutocomplete}>
        {queryProviderContext.suggestionsList.map((item: any, index: any) => (
          <AutocompleteItem
            key={index}
            isAlreadyViewed={item.viewed}
            onClick={() => {
              onAutocompleteItemClick(item);
            }}
            onClear={() => {
              clearItemSelection(item.title);
            }}
          >
            {item.title}
          </AutocompleteItem>
        ))}
      </AutocompleteList>
    </div>
  );
};
