import './styles.css';

export const AutocompleteList = ({
  children,
  shouldHideAutocompleteList,
  ...rest
}: {
  children: any;
  shouldHideAutocompleteList: boolean;
}) => {
  return (
    <ul
      className={`autocompleteList ${
        shouldHideAutocompleteList ? 'hideAutocomplete' : 'showAutocomplete'
      }`}
      {...rest}
    >
      {children}
    </ul>
  );
};
